import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Semesters from './views/Semesters.vue';
import Weeks from './views/Weeks.vue';
import ScheduleEditor from './views/ScheduleEditor.vue';
import ScheduleRooms from './views/ScheduleRooms.vue';
import ScheduleAnalysis from './views/ScheduleAnalysis.vue';
import SettingsRooms from './views/SettingsRooms.vue';
import SettingsTeachers from './views/SettingsTeachers.vue';
import SettingsGroups from './views/SettingsGroups.vue';
import SettingsExportExcel from './views/SettingsExportExcel.vue';
import Report from './views/Report.vue';
import Help from './views/Help.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/семестры',
      name: 'semesters',
      component: Semesters
    },
    {
      path: '/расписание-кабинеты',
      name: 'schedule-rooms',
      component: ScheduleRooms
    },
    {
      path: '/расписание-анализ',
      name: 'schedule-analysis',
      component: ScheduleAnalysis
    },
    {
      path: '/семестры/недели',
      name: 'weeks',
      component: Weeks,
    },
    {
      path: '/редактор-расписания',
      name: 'schedule-editor',
      component: ScheduleEditor,
    },
    {
      path: '/настройки/кабинеты',
      name: 'settings-rooms',
      component: SettingsRooms
    },
    {
      path: '/настройки/учителя',
      name: 'settings-teachers',
      component: SettingsTeachers
    },
    {
      path: '/настройки/группы',
      name: 'settings-groups',
      component: SettingsGroups
    },
    {
      path: '/настройки/экспорт-эксель',
      name: 'settings-export-excel',
      component: SettingsExportExcel
    },
    {
      path:'/ошибки',
      name: 'report',
      component: Report
    },
    {
      path:'/помощь',
      name: 'help',
      component: Help
    },
  ]
})
