import Vue from 'vue';
import Vuex from 'vuex';
import alert from './modules/alert.js';
import schedule from './modules/schedule.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {},
  getters: {
    // eslint-disable-next-line
    dateToDatabase: state => date => {
      let tempDate = new Date(date);
      return `${tempDate.getFullYear()}-${tempDate.getMonth()+1}-${tempDate.getDate()}`
    }
  },
  mutations: {
    
  },
  actions: {},
  modules: {
    alert,
    schedule,
  },
});