const state = {
    alertShow: false,
    alertTitle: 'хуй',
    alertMessage: '',
    alertAnswer: null,
};
const getters = {
    GetAlertInformation: state => {
        return {
            'alertShow':state.alertShow,
            'alertTitle':state.alertTitle,
            'alertMessage':state.alertMessage
        }
    },
    GetAlertAnswer: state =>{
        return state.alertAnswer;
    }
};
const mutations = {
    ReturnAlertAnswer: (state,payload) => {
        state.alertShow = false;
        state.alertTitle = '';
        state.alertMessage = '';
        state.alertAnswer = payload;
    },

    ShowAlert: (state,payload) => {//принимает Title, Message
      state.alertTitle = payload['title'];
      state.alertMessage = payload['message'];
      state.alertShow = true;
    },
 
    TakeAlertAnswer: state => {state.alertAnswer = null}
};
const actions = {};

export default {
  state,
  getters,
  mutations,
  actions,
};