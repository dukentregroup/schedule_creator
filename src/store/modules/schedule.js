import Vue from 'vue';
const state = {
    semester: null,
    week: null,
    weekDays: null,
    groups: [],
    openGroups: {},//names => true
    rowsHeight: [30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30],
};
const getters = {
    GetSemester: state => {
        return state.semester;
    },
    GetWeek: state => {
        return state.week;
    },
    GetGroups: state => {
        return state.groups;
    },
    GetWeekDays: state => {
        return state.weekDays;
    },
    GetRowHeight: state => field => {
        return state.rowsHeight[field];
    },
    GetOpenGroups: state => {
        return state.openGroups;
    }

};
const mutations = {
    SetSemester: (state, payload) => {
        state.semester = +payload;
    },
    SetWeek: (state, payload) => {
        state.week = +payload;
    },
    SetGroups: (state, payload) => {
        state.groups = payload;
    },
    SetWeekDays: (state, payload) => {
        state.weekDays = payload;
    },
    SetRowHeight: (state, payload) => {
        state.rowsHeight.splice(payload.rowNum, 1, payload.height);
        //state.fieldsHeight[payload.rowNum] = payload.height;
    },
    ResetRowsHeight: (state) => {
        state.rowsHeight = [];
        for (let i = 0; i < 41; i++) {
            state.rowsHeight.push(30);
        }
    },
    AddOpenGroup: (state, groupName) => {
        let obj = {};
        Object.assign(obj,state.openGroups);
        obj[groupName] = true;
        state.openGroups = obj;
    },
    ClearOpenGroups: (state) =>{
        state.openGroups = {};
    },
    RemoveOpenGroup: (state,groupName) =>{
        let obj = {};
        delete state.openGroups[groupName];
        Object.assign(obj,state.openGroups);
        //eslint-disable-next-line
        console.log("delete",groupName,obj);
        state.openGroups = obj;
    }

};
const actions = {};

export default {
    state,
    getters,
    mutations,
    actions,
};